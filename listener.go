package main

import (
	"flag"
	"fmt"
	"log"

	"code.google.com/p/gopacket"
	"code.google.com/p/gopacket/layers"
	"code.google.com/p/gopacket/pcap"
	"code.google.com/p/gopacket/tcpassembly"
)

var iface = flag.String("i", "eth0", "Interface to get packets from")
var snaplen = flag.Int("s", 65536, "SnapLen for pcap packet capture")
var port = flag.Int("port", 9876, "TCP port to listen on")

// factory for streams.
type factory struct{}

// bidirectionalStream consolidates packet data from both directions of the
// stream.
type bidirectionalStream struct {
	key        key
	references int
	messages   chan string
}

type key [2]gopacket.Flow

// unidirectionalStream forwards messages on to bidirectionalStream.
type unidirectionalStream struct {
	*bidirectionalStream
}

var bidirectionalMap = map[key]*bidirectionalStream{}

func (f *factory) New(net, transport gopacket.Flow) tcpassembly.Stream {
	log.Printf("new unidirectional stream %v:%v started", net, transport)
	s := &unidirectionalStream{}
	if b, ok := bidirectionalMap[key{net, transport}]; ok {
		s.bidirectionalStream = b
	} else if b, ok := bidirectionalMap[key{net.Reverse(), transport.Reverse()}]; ok {
		s.bidirectionalStream = b
	} else {
		log.Printf("new bidirectional stream %v:%v started", net, transport)
		bidi := &bidirectionalStream{
			key:      key{net, transport},
			messages: make(chan string),
		}
		s.bidirectionalStream = bidi
		bidirectionalMap[bidi.key] = bidi
		go bidi.handleMessages()
	}
	s.references++
	return s
}

func (s *unidirectionalStream) Reassembled(reassemblies []tcpassembly.Reassembly) {
	for _, reassembly := range reassemblies {
		switch len(reassembly.Bytes) {
		case 0:
			continue
		case 4:
			s.messages <- string(reassembly.Bytes)
		default:
			// I should actually buffer until I have a full message, but instead I'll
			// be lazy and just ignore things that aren't the right size
		}
	}
}

func (s *unidirectionalStream) ReassemblyComplete() {
	s.references--
	if s.references == 0 {
		close(s.messages)
		delete(bidirectionalMap, s.key)
	}
}

func (s *bidirectionalStream) handleMessages() {
	for msg := range s.messages {
		log.Printf("got msg %q on stream %v", msg, s.key)
	}
}

func main() {
	flag.Parse()

	log.Printf("starting capture on interface %q", *iface)
	// Set up pcap packet capture
	handle, err := pcap.OpenLive(*iface, int32(*snaplen), true, pcap.BlockForever)
	if err != nil {
		log.Fatal("error opening pcap handle: ", err)
	}
	if err := handle.SetBPFFilter(fmt.Sprintf("tcp and port %d", *port)); err != nil {
		log.Fatal("error setting BPF filter: ", err)
	}

	// Set up assembly
	streamFactory := &factory{}
	streamPool := tcpassembly.NewStreamPool(streamFactory)
	assembler := tcpassembly.NewAssembler(streamPool)
	assembler.MaxBufferedPagesPerConnection = 10
	defer assembler.FlushAll()

	log.Println("reading in packets")
	source := gopacket.NewPacketSource(handle, layers.LayerTypeEthernet)

	for packet := range source.Packets() {
		net := packet.NetworkLayer()
		tcp := packet.Layer(layers.LayerTypeTCP)
		if net == nil || tcp == nil {
			log.Printf("no network or tcp layer:\n%v", packet)
			continue
		}
		assembler.Assemble(net.NetworkFlow(), tcp.(*layers.TCP))
	}
}
