package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"net"
	"time"
)

var mode = flag.String("mode", "client", "One of 'client' or 'server'")
var addr = flag.String("addr", "", "Remote TCP server to connect to (host:port), if client")
var port = flag.Int("port", 9876, "Port to bind to, if server")
var timeoutMicros = flag.Int("timeout", 1000, "micros to wait between ping/pong")

func main() {
	flag.Parse()
	switch *mode {
	case "client":
		client()
	case "server":
		server()
	default:
		log.Fatalf("invalid mode %v", *mode)
	}
}

func wait() {
	time.Sleep(time.Duration(*timeoutMicros) * time.Microsecond)
}

func server() {
	log.Printf("running server on port %d", *port)
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("could not bind port %v: %v", *port, err)
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("error accepting: %v", err)
			continue
		}
		go serverConn(conn)
	}
}

func serverConn(conn net.Conn) {
	log.Printf("opened conn to %v", conn.RemoteAddr())
	defer func() {
		log.Printf("closing conn to %v", conn.RemoteAddr())
		conn.Close()
	}()
	buf := make([]byte, 4)
	for {
		if in, err := conn.Read(buf); err != nil {
			log.Printf("error reading: %v", err)
			return
		} else if in != 4 {
			log.Printf("not enough bytes: %v", in)
			return // should actually block waiting for more, but eh.
		} else if !bytes.Equal(buf, []byte("PING")) {
			log.Printf("got non-ping %q", string(buf))
			return
		}
		log.Printf("got ping")
		wait()
		if _, err := conn.Write([]byte("PONG")); err != nil {
			log.Printf("could not write pong: %v", err)
		}
	}
}

func client() {
	log.Printf("starting client of remote address %q", *addr)
	conn, err := net.Dial("tcp", *addr)
	if err != nil {
		log.Fatalf("could not connect to %q: %v", *addr, err)
	}
	log.Printf("opened conn to %v", conn.RemoteAddr())
	defer func() {
		log.Printf("closing conn to %v", conn.RemoteAddr())
		conn.Close()
	}()
	buf := make([]byte, 4)
	for {
		if _, err := conn.Write([]byte("PING")); err != nil {
			log.Printf("could not write ping: %v", err)
		}
		if in, err := conn.Read(buf); err != nil {
			log.Printf("error reading: %v", err)
			return
		} else if in != 4 {
			log.Printf("not enough bytes: %v", in)
			return // should actually block waiting for more, but eh.
		} else if !bytes.Equal(buf, []byte("PONG")) {
			log.Printf("got non-pong %q", string(buf))
			return
		}
		log.Printf("got pong")
		wait()
	}
}
